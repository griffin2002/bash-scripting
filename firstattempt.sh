#! /bin/bash

## Today we'll be installing the awscli for Linux
## we need to find the location of the latest awscli file
## initial search reveals the file is located here
## https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip
## in order to download it we can use two programs to pull
## the file from the internet

## Option 1: Curl ** direct from the Amazon
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

## Option 2: WGET

wget https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip

## now we have a zip file that needs to be extracted
unzip awscliv2.zip

## now that all the files are extracted within the aws
## subdirectory we will need to run the install command

./aws/install
## the above will install the awscli on the current machine

## in order to validate that the commands have executed successfully we can
## a simple version command to ensure that the installation completed

aws --version
