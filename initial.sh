#! /bin/bash

## add a program to the system

## where to install the program for everyone to use it?
## where to install for only the original user to use it

## how to download the file from the internet
## what to do with the file that you receive?

## modify the permissions of the file so it can execute

## find the path to the file you need to use
## either add the folder to the path OR create an alias within your .bashrc file

## run the command to verify that the script has executed successfully
